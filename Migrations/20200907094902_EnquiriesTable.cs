﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PropertySite.Migrations
{
    public partial class EnquiriesTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Enquiry_Properties_PropertyID",
                table: "Enquiry");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Enquiry",
                table: "Enquiry");

            migrationBuilder.RenameTable(
                name: "Enquiry",
                newName: "Enquiries");

            migrationBuilder.RenameIndex(
                name: "IX_Enquiry_PropertyID",
                table: "Enquiries",
                newName: "IX_Enquiries_PropertyID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Enquiries",
                table: "Enquiries",
                columns: new[] { "ID", "PropertyID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Enquiries_Properties_PropertyID",
                table: "Enquiries",
                column: "PropertyID",
                principalTable: "Properties",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Enquiries_Properties_PropertyID",
                table: "Enquiries");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Enquiries",
                table: "Enquiries");

            migrationBuilder.RenameTable(
                name: "Enquiries",
                newName: "Enquiry");

            migrationBuilder.RenameIndex(
                name: "IX_Enquiries_PropertyID",
                table: "Enquiry",
                newName: "IX_Enquiry_PropertyID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Enquiry",
                table: "Enquiry",
                columns: new[] { "ID", "PropertyID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Enquiry_Properties_PropertyID",
                table: "Enquiry",
                column: "PropertyID",
                principalTable: "Properties",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
