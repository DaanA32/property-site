﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PropertySite.Migrations
{
    public partial class RenameID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Enquiries",
                table: "Enquiries");

            migrationBuilder.DropColumn(
                name: "ID",
                table: "Enquiries");

            migrationBuilder.AddColumn<Guid>(
                name: "EnquiryID",
                table: "Enquiries",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_Enquiries",
                table: "Enquiries",
                columns: new[] { "EnquiryID", "PropertyID" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Enquiries",
                table: "Enquiries");

            migrationBuilder.DropColumn(
                name: "EnquiryID",
                table: "Enquiries");

            migrationBuilder.AddColumn<Guid>(
                name: "ID",
                table: "Enquiries",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_Enquiries",
                table: "Enquiries",
                columns: new[] { "ID", "PropertyID" });
        }
    }
}
