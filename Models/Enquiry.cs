using System;
using System.ComponentModel.DataAnnotations;

namespace PropertySite.Models
{
    public class Enquiry
    {
        [Required]
        public Guid EnquiryID { get; set; }

        [Required]

        public string Name { get; set; }

        [Required]
        public string Text { get; set; }

        [Required]
        public Guid PropertyID { get; set; }
        public Property ParentProperty { get; set; }

        [Display(Name = "Created Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreatedDate { get; set; }
    }
}
