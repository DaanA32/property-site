using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PropertySite.Models
{
    public class Property
    {
        public Guid ID { get; set; }
        public string Description { get; set; }
        public uint Rent { get; set; }

        [InverseProperty("ParentProperty")]
        public List<Enquiry> Enquiries { get; set; }

        [Display(Name = "Created Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreatedDate { get; set; }

        [Required]
        public string UserID { get; set; }
        public User ParentUser { get; set; }
    }
}
