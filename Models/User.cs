using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PropertySite.Models
{
    public class User : IdentityUser
    {

        [InverseProperty("ParentUser")]
        public List<Property> Properties { get; set; }

    }
}