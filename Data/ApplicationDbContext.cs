﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PropertySite.Models;
using Microsoft.AspNetCore.Identity;

namespace PropertySite.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<Property> Properties { get; set; }
        public DbSet<Enquiry> Enquiries { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IdentityUserLogin<string>>().HasKey(e => e.UserId);
            modelBuilder.Entity<IdentityUserRole<string>>().HasKey(e => e.UserId);
            modelBuilder.Entity<IdentityUserToken<string>>().HasKey(e => e.UserId);

            modelBuilder.Entity<Enquiry>().HasKey(e => new {e.EnquiryID, e.PropertyID});
            modelBuilder.Entity<Enquiry>().Property(e => e.EnquiryID).ValueGeneratedOnAdd();

            modelBuilder.Entity<Property>().HasMany(e => e.Enquiries).WithOne(e => e.ParentProperty);

        }
    }
}
