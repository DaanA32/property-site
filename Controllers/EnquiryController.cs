using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using PropertySite.Models;
using PropertySite.Data;

namespace PropertySite.Controllers
{
    public class EnquiryController : Controller
    {
        private readonly ILogger<EnquiryController> _logger;
        private readonly ApplicationDbContext _context;

        public EnquiryController(ILogger<EnquiryController> logger, ApplicationDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task<IActionResult> Create(Guid? id)
        {
            if (id == null){
                return NotFound();
            }

            var property = await _context.Properties.FirstOrDefaultAsync(m => m.ID == id);
            if (property == null) {
                return NotFound();
            }
            return View(new Enquiry() {
                EnquiryID= Guid.NewGuid(),
                PropertyID = id.Value,
                CreatedDate = DateTime.Now,
            });
        }
    
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EnquiryID,PropertyID,Name,Text,CreatedDate")] Enquiry enquiry)
        {
            if (ModelState.IsValid) {
                Console.Out.WriteLine($"{enquiry.EnquiryID}, {enquiry.PropertyID}");
                _context.Enquiries.Add(enquiry);
                await _context.SaveChangesAsync();
                return Redirect($"/Home/Details/{enquiry.PropertyID}");
            }
            return View(enquiry);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
