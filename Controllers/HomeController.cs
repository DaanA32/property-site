﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using PropertySite.Models;
using PropertySite.Data;
using System.Security.Claims;

namespace PropertySite.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDbContext _context;

        public HomeController(ILogger<HomeController> logger, ApplicationDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _context.Properties.ToListAsync());
        }

        public IActionResult Create()
        {
            var userId =  User.FindFirst(ClaimTypes.NameIdentifier);
            return View(new Property() {
                UserID = userId.Value,
                CreatedDate = DateTime.Now,
            });
        }
    
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Create([Bind("UserID,ID,Description,Rent,CreatedDate")] Property property)
        {
            if (ModelState.IsValid) {
                _context.Add(property);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(property);
        }

        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null){
                return NotFound();
            }

            var property = await _context.Properties.Include(m => m.Enquiries).FirstOrDefaultAsync(m => m.ID == id);
            if (property == null) {
                return NotFound();
            }

            return View(property);
        }

        [Authorize]
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null) {
                return NotFound();
            }

            var property = await _context.Properties.FindAsync(id);
            if (property == null) {
                return NotFound();
            }
            var userId =  User.FindFirst(ClaimTypes.NameIdentifier);
            if (property.UserID != userId.Value) {
                return Unauthorized();
            }
            return View(property);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("ID,UserID,Description,Rent,CreatedDate")] Property property)
        {
            if (id != property.ID) {
                return NotFound();
            }
            var userId =  User.FindFirst(ClaimTypes.NameIdentifier);
            if (property.UserID != userId.Value) {
                return Unauthorized();
            }

            if (ModelState.IsValid) {
                try {
                    _context.Update(property);
                    await _context.SaveChangesAsync();
                } catch (DbUpdateConcurrencyException) {
                    if (!PropertiesExists(property.ID)) {
                        return NotFound();
                    } else {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(property);
        }

        [Authorize]
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null) {
                return NotFound();
            }

            var property = await _context.Properties.FirstOrDefaultAsync(m => m.ID == id);
            if (property == null) {
                return NotFound();
            }
            var userId =  User.FindFirst(ClaimTypes.NameIdentifier);
            if (property.UserID != userId.Value) {
                return Unauthorized();
            }

            return View(property);
        }

        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var userId =  User.FindFirst(ClaimTypes.NameIdentifier);
            var property = await _context.Properties.FindAsync(id);
            if (property.UserID != userId.Value) {
                return Unauthorized();
            }
            _context.Properties.Remove(property);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PropertiesExists(Guid id)
        {
            return _context.Properties.Any(e => e.ID == id);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
